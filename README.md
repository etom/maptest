# MapTest

これは、Android で Googl Map を表示するテストプログラムです。  
local.properties に MAPS_API_KEY= の設定が必要です。

起動すると location API で現在地を取得し、Map で表示するサンプルです。

参考：
- location:
  - https://developer.android.com/training/location/retrieve-current?hl=ja
  - https://stackoverflow.com/questions/72159435/how-to-get-location-using-fusedlocationclient-getcurrentlocation-method-in-kot
- map:
  - https://developers.google.com/maps/documentation/android-sdk/map?hl=ja
